#***********************************************

FROM golang:1.15.3-alpine3.12 as builder

WORKDIR /go/src/gitlab.com/lucasgranade/mazeapi

COPY src ./

RUN go build -o /go/bin/mazeapi

#***********************************************

FROM golang:1.15.3-alpine3.12 as runner

WORKDIR /opt/mazeapi/

COPY --from=builder \
  /go/bin/mazeapi \
  .

EXPOSE 8080

CMD ["/opt/mazeapi/mazeapi"]

#***********************************************