package main

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"

	"gitlab.com/lucasgranade/mazeapi/controller"
)

func createRoutes() (chi.Router, error) {
	r := chi.NewRouter()

	// make it work from any domain on the browser
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"*"},
		ExposedHeaders:   []string{},
		AllowCredentials: true,
		MaxAge:           600,
	})

	r.Use(
		render.SetContentType(render.ContentTypeJSON),
		middleware.RequestID,
		middleware.RedirectSlashes,
		cors.Handler,
		middleware.RequestLogger(&httpLogger{}),
	)

	r.Post("/mazes", controller.CreateMaze)
	r.Get("/mazes/{mazeID}", controller.ReadMazeByID)
	r.Put("/mazes/{mazeID}", controller.ModifyMaze)
	r.Delete("/mazes/{mazeID}", controller.DeleteMaze)

	r.Post("/mazes/{mazeID}/spots", controller.CreateSpot)
	r.Get("/mazes/{mazeID}/spots/{spotID}", controller.ReadSpotByID)
	r.Delete("/mazes/{mazeID}/spots/{spotID}", controller.DeleteSpot)

	r.Get("/mazes/{mazeID}/mindistance", controller.GetMinDistanceRoute)
	r.Get("/mazes/{mazeID}/maxgold", controller.GetMaxGoldRoute)

	return r, nil
}
