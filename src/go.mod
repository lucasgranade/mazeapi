module gitlab.com/lucasgranade/mazeapi

go 1.15

require (
	github.com/AlekSi/pointer v1.1.0
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/render v1.0.1
	github.com/google/uuid v1.1.1
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.4.3
)
