// +build unit

package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/lucasgranade/mazeapi/dao"
	"gitlab.com/lucasgranade/mazeapi/model"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/stretchr/testify/assert"
)

type postMazeTest struct {
	reqBody     string
	statusCode  int
	mazeCreated bool
}

var postMazeTests = map[string]postMazeTest{
	"OK": postMazeTest{
		`
		{
			"name": "minotaur",
			"center": {
				"x": 0,
				"y": 0
			}
		}
		`,
		201,
		true,
	},
	"NoCenter": postMazeTest{
		`
		{
			"name": "minotaur"
		}
		`,
		400,
		false,
	},
	"NoCenterX": postMazeTest{
		`
		{
			"name": "minotaur",
			"center": {
				"y": 0
			}
		}
		`,
		400,
		false,
	},
	"NoName": postMazeTest{
		`
		{
			"center": {
				"x": 0,
				"y": 0
			}
		}
		`,
		400,
		false,
	},
	"BadName": postMazeTest{
		`
		{
			"name": 2,
			"center": {
				"x": 0,
				"y": 0
			}
		}
		`,
		400,
		false,
	},
}

func TestPostMaze(t *testing.T) {

	for k, v := range postMazeTests {
		t.Run(k, func(it *testing.T) {

			mazeCreated := false

			dao.Set(&dao.MazeMockupImpl{
				CreateMazeHandler: func(ctx context.Context, name string, center model.Center) (*model.Maze, error) {
					mazeCreated = true
					return &model.Maze{
						ID:     primitive.NewObjectID(),
						Name:   name,
						Center: center,
					}, nil
				},
			})

			handler, err := createRoutes()
			if err != nil {
				it.Errorf("%w", err)
				return
			}

			req := httptest.NewRequest(http.MethodPost, fmt.Sprintf("https://api.ovloop.com/mazes"), nil)
			req.Body = ioutil.NopCloser(bytes.NewReader([]byte(v.reqBody)))

			w := httptest.NewRecorder()
			handler.ServeHTTP(w, req)
			resp := w.Result()

			assert.Equal(it, v.statusCode, resp.StatusCode, "bad return status code")
			assert.Equal(it, v.mazeCreated, mazeCreated, "bad maze created flag")

			if resp.StatusCode == http.StatusCreated {
				decoder := json.NewDecoder(resp.Body)
				maze := model.Maze{}
				err = decoder.Decode(&maze)
				if err != nil {
					it.Errorf("Couldn't parse body, err: %v", err)
					return
				}
			}
		})
	}
}

type getMazeTest struct {
	mazeID     string
	exists     bool
	statusCode int
	mazeRead   bool
}

var getMazeTests = map[string]getMazeTest{
	"OK": getMazeTest{
		"5fbf3dab715e12693d8d6076",
		true,
		200,
		true,
	},
	"BadMazeID": getMazeTest{
		"sasa",
		false,
		400,
		false,
	},
	"MazeNotFound": getMazeTest{
		"5fbf3dab715e12693d8d6076",
		false,
		404,
		true,
	},
}

func TestGetMaze(t *testing.T) {

	for k, v := range getMazeTests {
		t.Run(k, func(it *testing.T) {

			mazeRead := false

			dao.Set(&dao.MazeMockupImpl{
				ReadMazeByIDHandler: func(ctx context.Context, mazeID primitive.ObjectID) (*model.Maze, error) {
					mazeRead = true
					if v.exists {
						return &model.Maze{
							ID:     mazeID,
							Name:   "maze",
							Center: model.Center{},
						}, nil
					}
					return nil, dao.ErrNotFound
				},
			})

			handler, err := createRoutes()
			if err != nil {
				it.Errorf("%w", err)
				return
			}

			req := httptest.NewRequest(http.MethodGet, fmt.Sprintf("https://api.ovloop.com/mazes/%s", v.mazeID), nil)

			w := httptest.NewRecorder()
			handler.ServeHTTP(w, req)
			resp := w.Result()

			assert.Equal(it, v.statusCode, resp.StatusCode, "bad return status code")
			assert.Equal(it, v.mazeRead, mazeRead, "bad maze read flag")

			if resp.StatusCode == http.StatusOK {
				decoder := json.NewDecoder(resp.Body)
				maze := model.Maze{}
				err = decoder.Decode(&maze)
				if err != nil {
					it.Errorf("Couldn't parse body, err: %v", err)
					return
				}
			}
		})
	}
}

type putMazeTest struct {
	mazeID       string
	reqBody      string
	exists       bool
	statusCode   int
	mazeModified bool
}

var putMazeTests = map[string]putMazeTest{
	"OK": putMazeTest{
		"5fbf3dab715e12693d8d6076",
		`
		{
			"center": {
				"x": 10,
				"y": 10
			}
		}
		`,
		true,
		200,
		true,
	},
	"MazeNotFound": putMazeTest{
		"5fbf3dab715e12693d8d6076",
		`
		{
			"center": {
				"x": 10,
				"y": 10
			}
		}
		`,
		false,
		404,
		false,
	},
	"BadMazeID": putMazeTest{
		"5fbf3d",
		`
		{
			"center": {
				"x": 10,
				"y": 10
			}
		}
		`,
		false,
		400,
		false,
	},
	"LackingCenterY": putMazeTest{
		"5fbf3dab715e12693d8d6076",
		`
		{
			"center": {
				"x": 10
			}
		}
		`,
		true,
		400,
		false,
	},
}

func TestPutMaze(t *testing.T) {

	for k, v := range putMazeTests {
		t.Run(k, func(it *testing.T) {

			mazeModified := false

			dao.Set(&dao.MazeMockupImpl{
				ModifyMazeHandler: func(ctx context.Context, mazeID primitive.ObjectID, center model.Center) (*model.Maze, error) {
					if v.exists {
						mazeModified = true
						return &model.Maze{
							ID:     mazeID,
							Name:   "minotaur",
							Center: center,
						}, nil
					}
					return nil, dao.ErrNotFound
				},
			})

			handler, err := createRoutes()
			if err != nil {
				it.Errorf("%w", err)
				return
			}

			req := httptest.NewRequest(http.MethodPut, fmt.Sprintf("https://api.ovloop.com/mazes/%s", v.mazeID), nil)
			req.Body = ioutil.NopCloser(bytes.NewReader([]byte(v.reqBody)))

			w := httptest.NewRecorder()
			handler.ServeHTTP(w, req)
			resp := w.Result()

			assert.Equal(it, v.statusCode, resp.StatusCode, "bad return status code")
			assert.Equal(it, v.mazeModified, mazeModified, "bad maze modified flag")

			if resp.StatusCode == http.StatusCreated {
				decoder := json.NewDecoder(resp.Body)
				maze := model.Maze{}
				err = decoder.Decode(&maze)
				if err != nil {
					it.Errorf("Couldn't parse body, err: %v", err)
					return
				}
			}
		})
	}
}

type deleteMazeTest struct {
	mazeID     string
	exists     bool
	statusCode int
}

var deleteMazeTests = map[string]deleteMazeTest{
	"OK": deleteMazeTest{
		"5fbf3dab715e12693d8d6076",
		true,
		200,
	},
	"BadMazeID": deleteMazeTest{
		"badID",
		false,
		400,
	},
	"MazeNotFound": deleteMazeTest{
		"5fbf3dab715e12693d8d6076",
		false,
		404,
	},
}

func TestDeleteMaze(t *testing.T) {

	for k, v := range deleteMazeTests {
		t.Run(k, func(it *testing.T) {

			dao.Set(&dao.MazeMockupImpl{
				ReadMazeByIDHandler: func(ctx context.Context, mazeID primitive.ObjectID) (*model.Maze, error) {
					if v.exists {
						return &model.Maze{
							ID:     mazeID,
							Name:   "maze",
							Center: model.Center{},
						}, nil
					}
					return nil, dao.ErrNotFound
				},
			})

			handler, err := createRoutes()
			if err != nil {
				it.Errorf("%w", err)
				return
			}

			req := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("https://api.ovloop.com/mazes/%s", v.mazeID), nil)

			w := httptest.NewRecorder()
			handler.ServeHTTP(w, req)
			resp := w.Result()

			assert.Equal(it, v.statusCode, resp.StatusCode, "bad return status code")
		})
	}
}
