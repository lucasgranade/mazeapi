package controller

import (
	"encoding/json"
	"errors"
	"net/http"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/go-chi/chi"
	"gitlab.com/lucasgranade/mazeapi/model"
	"gitlab.com/lucasgranade/mazeapi/service"
)

type mazeCenterReq struct {
	X *float64 `json:"x,omitempty"`
	Y *float64 `json:"y,omitempty"`
}

type createMazeReq struct {
	Name   string        `json:"name,omitempty"`
	Center mazeCenterReq `json:"center,omitempty"`
}

// CreateMaze creates a new maze
func CreateMaze(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	reqB := createMazeReq{}
	err := decoder.Decode(&reqB)
	if err != nil {
		apiError(w, r, http.StatusBadRequest, "Bad formed request body", IErrorIllFormedRequest)
		return
	}

	if reqB.Name == "" {
		apiError(w, r, http.StatusBadRequest, "Missing maze name", IErrorLackingFields)
		return
	}
	if reqB.Center.X == nil || reqB.Center.Y == nil {
		apiError(w, r, http.StatusBadRequest, "Missing maze center configuration", IErrorLackingFields)
		return
	}

	maze, err := service.CreateMaze(r.Context(), reqB.Name,
		model.Center{
			X: *reqB.Center.X,
			Y: *reqB.Center.Y,
		})
	if err != nil {
		apiError(w, r, http.StatusInternalServerError, "Couldn't create the game session, report error", 0)
		return
	}

	apiResponse(w, r, http.StatusCreated, maze)
}

// ReadMazeByID reads a maze
func ReadMazeByID(w http.ResponseWriter, r *http.Request) {
	mazeID, err := primitive.ObjectIDFromHex(chi.URLParam(r, "mazeID"))
	if err != nil {
		apiError(w, r, http.StatusBadRequest, "Bad maze id", IErrorIllFormedRequest)
		return
	}

	maze, err := service.ReadMazeByID(r.Context(), mazeID)
	if err != nil {
		if errors.Is(err, service.ErrNonexistentMaze) {
			apiError(w, r, http.StatusNotFound, "Nonexistent maze", IErrorNonexistentMaze)
			return
		}
		apiError(w, r, http.StatusInternalServerError, "Couldn't create the game session, report error", 0)
		return
	}

	apiResponse(w, r, http.StatusOK, maze)
}

type modifyMazeReq struct {
	Center mazeCenterReq `json:"center,omitempty"`
}

// ModifyMaze modifies maze
func ModifyMaze(w http.ResponseWriter, r *http.Request) {
	mazeID, err := primitive.ObjectIDFromHex(chi.URLParam(r, "mazeID"))
	if err != nil {
		apiError(w, r, http.StatusBadRequest, "Bad maze id", IErrorIllFormedRequest)
		return
	}

	decoder := json.NewDecoder(r.Body)
	reqB := modifyMazeReq{}
	err = decoder.Decode(&reqB)
	if err != nil {
		apiError(w, r, http.StatusBadRequest, "Bad formed request body", IErrorIllFormedRequest)
		return
	}
	if reqB.Center.X == nil || reqB.Center.Y == nil {
		apiError(w, r, http.StatusBadRequest, "Missing maze center configuration", IErrorLackingFields)
		return
	}

	maze, err := service.ModifyMaze(r.Context(), mazeID,
		model.Center{
			X: *reqB.Center.X,
			Y: *reqB.Center.Y,
		})
	if err != nil {
		if errors.Is(err, service.ErrNonexistentMaze) {
			apiError(w, r, http.StatusNotFound, "Nonexistent maze", IErrorNonexistentMaze)
			return
		}
		apiError(w, r, http.StatusInternalServerError, "Report error", 0)
		return
	}

	apiResponse(w, r, http.StatusOK, maze)
}

// DeleteMaze removes a maze
func DeleteMaze(w http.ResponseWriter, r *http.Request) {
	mazeID, err := primitive.ObjectIDFromHex(chi.URLParam(r, "mazeID"))
	if err != nil {
		apiError(w, r, http.StatusBadRequest, "Bad maze id", IErrorIllFormedRequest)
		return
	}

	err = service.DeleteMaze(r.Context(), mazeID)
	if err != nil {
		if errors.Is(err, service.ErrNonexistentMaze) {
			apiError(w, r, http.StatusNotFound, "Nonexistent maze", IErrorNonexistentMaze)
			return
		}
		apiError(w, r, http.StatusInternalServerError, "Couldn't create the maze, report error", 0)
		return
	}

	apiResponse(w, r, http.StatusOK, nil)
}
