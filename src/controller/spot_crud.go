package controller

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/lucasgranade/mazeapi/model"
	"gitlab.com/lucasgranade/mazeapi/service"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type createSpotReq struct {
	Name     string                `json:"name"`
	X        *float64              `json:"x"`
	Y        *float64              `json:"y"`
	Gold     *float64              `json:"gold"`
	Connects []model.ConnectedSpot `json:"connects"`
}

// CreateSpot creates a new spot
func CreateSpot(w http.ResponseWriter, r *http.Request) {
	mazeID, err := primitive.ObjectIDFromHex(chi.URLParam(r, "mazeID"))
	if err != nil {
		apiError(w, r, http.StatusBadRequest, "Bad maze id", IErrorIllFormedRequest)
		return
	}

	decoder := json.NewDecoder(r.Body)
	reqB := createSpotReq{}
	err = decoder.Decode(&reqB)
	if err != nil {
		apiError(w, r, http.StatusBadRequest, "Couldn't parse body", IErrorIllFormedRequest)
		return
	}

	if reqB.Name == "" {
		apiError(w, r, http.StatusBadRequest, "Missing spot name", IErrorLackingFields)
		return
	}

	if reqB.X == nil || reqB.Y == nil {
		apiError(w, r, http.StatusBadRequest, "Missing spot position", IErrorLackingFields)
		return
	}

	if reqB.Gold == nil {
		apiError(w, r, http.StatusBadRequest, "Missing gold at spot", IErrorLackingFields)
		return
	}

	if *reqB.Gold < 0 {
		apiError(w, r, http.StatusBadRequest, "Gold must be a positive number", IErrorBadParameter)
		return
	}

	for i, conn := range reqB.Connects {
		if conn.Path <= 0 {
			apiError(w, r, http.StatusBadRequest, fmt.Sprintf("connected spot at index %d must have path bigger than 0", i), IErrorBadParameter)
			return
		}
		if conn.Spot == "" {
			apiError(w, r, http.StatusBadRequest, fmt.Sprintf("connected spot at index %d must have name", i), IErrorBadParameter)
			return
		}
	}

	maze, err := service.CreateSpot(
		r.Context(),
		mazeID,
		reqB.Name,
		*reqB.X,
		*reqB.Y,
		*reqB.Gold,
		reqB.Connects,
	)
	if err != nil {
		if errors.Is(err, service.ErrNonexistentMaze) {
			apiError(w, r, http.StatusNotFound, "Nonexistent maze", IErrorNonexistentMaze)
			return
		} else if errors.Is(err, service.ErrDuplicateSpot) {
			apiError(w, r, http.StatusConflict, "Duplicate spot", IErrorDuplicateSpot)
			return
		}
		apiError(w, r, http.StatusInternalServerError, "Couldn't create the spot in the maze, report error", 0)
		return
	}

	apiResponse(w, r, http.StatusCreated, maze)
}

// ReadSpotByID reads a spot
func ReadSpotByID(w http.ResponseWriter, r *http.Request) {
	mazeID, err := primitive.ObjectIDFromHex(chi.URLParam(r, "mazeID"))
	if err != nil {
		apiError(w, r, http.StatusBadRequest, "Bad maze id", IErrorIllFormedRequest)
		return
	}

	spotID, err := primitive.ObjectIDFromHex(chi.URLParam(r, "spotID"))
	if err != nil {
		apiError(w, r, http.StatusBadRequest, "Bad spot id", IErrorIllFormedRequest)
		return
	}

	spot, err := service.ReadSpotByID(r.Context(), mazeID, spotID)
	if err != nil {
		if errors.Is(err, service.ErrNonexistentMaze) {
			apiError(w, r, http.StatusNotFound, "Nonexistent maze", IErrorNonexistentMaze)
			return
		} else if errors.Is(err, service.ErrNonexistentSpot) {
			apiError(w, r, http.StatusNotFound, "Nonexistent spot", IErrorNonexistentSpot)
			return
		}
		apiError(w, r, http.StatusInternalServerError, "Report error", 0)
		return
	}

	apiResponse(w, r, http.StatusOK, spot)
}

// DeleteSpot removes a spot from a maze
func DeleteSpot(w http.ResponseWriter, r *http.Request) {
	mazeID, err := primitive.ObjectIDFromHex(chi.URLParam(r, "mazeID"))
	if err != nil {
		apiError(w, r, http.StatusBadRequest, "Bad maze id", IErrorIllFormedRequest)
		return
	}

	spotID, err := primitive.ObjectIDFromHex(chi.URLParam(r, "spotID"))
	if err != nil {
		apiError(w, r, http.StatusBadRequest, "Bad spot id", IErrorIllFormedRequest)
		return
	}

	err = service.DeleteSpot(r.Context(), mazeID, spotID)
	if err != nil {
		if errors.Is(err, service.ErrNonexistentMaze) {
			apiError(w, r, http.StatusNotFound, "Nonexistent maze", IErrorNonexistentMaze)
			return
		} else if errors.Is(err, service.ErrNonexistentSpot) {
			apiError(w, r, http.StatusNotFound, "Nonexistent spot", IErrorNonexistentSpot)
			return
		}
		apiError(w, r, http.StatusInternalServerError, "Couldn't create the spot in the maze, report error", 0)
		return
	}

	apiResponse(w, r, http.StatusOK, nil)
}
