package controller

import (
	"errors"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/lucasgranade/mazeapi/service"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// GetMinDistanceRoute searches for the path with minimum walking distance from spot A to spot B
func GetMinDistanceRoute(w http.ResponseWriter, r *http.Request) {
	mazeID, err := primitive.ObjectIDFromHex(chi.URLParam(r, "mazeID"))
	if err != nil {
		apiError(w, r, http.StatusBadRequest, "Bad maze id", IErrorIllFormedRequest)
		return
	}

	fromSpot := r.URL.Query().Get("from")
	if fromSpot == "" {
		apiError(w, r, http.StatusBadRequest, "Must indicate from spot in query", IErrorLackingFields)
		return
	}

	toSpot := r.URL.Query().Get("to")
	if toSpot == "" {
		apiError(w, r, http.StatusBadRequest, "Must indicate to spot in query", IErrorLackingFields)
		return
	}

	route, err := service.FindMinDistanceRoute(r.Context(), mazeID, fromSpot, toSpot)
	if err != nil {
		if errors.Is(err, service.ErrNonexistentMaze) {
			apiError(w, r, http.StatusNotFound, "Nonexistent maze", IErrorNonexistentMaze)
			return
		} else if errors.Is(err, service.ErrNonexistentSpot) {
			apiError(w, r, http.StatusNotFound, "Nonexistent spot", IErrorNonexistentSpot)
			return
		}
		apiError(w, r, http.StatusInternalServerError, "Couldn't find best route, report error", 0)
		return
	}

	apiResponse(w, r, http.StatusOK, route)
}

// GetMaxGoldRoute searches for the path from spot A to spot B getting the most amount of gold
func GetMaxGoldRoute(w http.ResponseWriter, r *http.Request) {
	mazeID, err := primitive.ObjectIDFromHex(chi.URLParam(r, "mazeID"))
	if err != nil {
		apiError(w, r, http.StatusBadRequest, "Bad maze id", IErrorIllFormedRequest)
		return
	}

	fromSpot := r.URL.Query().Get("from")
	if fromSpot == "" {
		apiError(w, r, http.StatusBadRequest, "Must indicate from spot in query", IErrorLackingFields)
		return
	}

	toSpot := r.URL.Query().Get("to")
	if toSpot == "" {
		apiError(w, r, http.StatusBadRequest, "Must indicate to spot in query", IErrorLackingFields)
		return
	}

	route, err := service.FindMaxGoldRoute(r.Context(), mazeID, fromSpot, toSpot)
	if err != nil {
		if errors.Is(err, service.ErrNonexistentMaze) {
			apiError(w, r, http.StatusNotFound, "Nonexistent maze", IErrorNonexistentMaze)
			return
		} else if errors.Is(err, service.ErrNonexistentSpot) {
			apiError(w, r, http.StatusNotFound, "Nonexistent spot", IErrorNonexistentSpot)
			return
		}
		apiError(w, r, http.StatusInternalServerError, "Couldn't find best route, report error", 0)
		return
	}

	apiResponse(w, r, http.StatusOK, route)
}
