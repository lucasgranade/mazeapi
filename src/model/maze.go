package model

import "go.mongodb.org/mongo-driver/bson/primitive"

// Center specifies the center of the maze
type Center struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}

// Maze is the public model for an actual maze
type Maze struct {
	ID     primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name   string             `json:"name"`
	Center Center             `json:"center"`
}
