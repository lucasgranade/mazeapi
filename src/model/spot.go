package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// ConnectedSpot is an item in the connected array on a spot public model
type ConnectedSpot struct {
	Spot string  `json:"spot"`
	Path float64 `json:"path"`
}

// Spot is a place inside the maze for the public model
type Spot struct {
	ID       primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	MazeID   primitive.ObjectID `json:"-" bson:"mazeId,omitempty"`
	Name     string             `json:"name"`
	X        float64            `json:"x"`
	Y        float64            `json:"y"`
	Gold     float64            `json:"gold"`
	Connects []ConnectedSpot    `json:"connects"`
	Quadrant string             `json:"quadrant"`
}

// CalculateQuadrant sets quadrant on spot
func (s *Spot) CalculateQuadrant(maze *Maze) {
	if s.X >= maze.Center.X && s.Y >= maze.Center.Y {
		s.Quadrant = UpperRight
	} else if s.X < maze.Center.X && s.Y >= maze.Center.Y {
		s.Quadrant = UpperLeft
	} else if s.X < maze.Center.X && s.Y < maze.Center.Y {
		s.Quadrant = LowerLeft
	} else {
		s.Quadrant = LowerRight
	}
}

const (
	// UpperRight indicates that the spot is in the upper right quadrant
	UpperRight = "upperRight"

	// UpperLeft indicates that the spot is in the upper left quadrant
	UpperLeft = "upperLeft"

	// LowerLeft indicates that the spot is in the lower left quadrant
	LowerLeft = "lowerLeft"

	// LowerRight indicates that the spot is in the lower right quadrant
	LowerRight = "lowerRight"
)
