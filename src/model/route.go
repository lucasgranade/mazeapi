package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// RouteSpot represents an item in the route array for the public model
type RouteSpot struct {
	Name string             `json:"name"`
	ID   primitive.ObjectID `json:"id"`
}

// Route represents the route returned in the public model
type Route struct {
	Distance float64     `json:"distance"`
	Gold     float64     `json:"gold"`
	Route    []RouteSpot `json:"route"`
}
