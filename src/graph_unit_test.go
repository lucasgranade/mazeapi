// +build unit

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/lucasgranade/mazeapi/dao"
	"gitlab.com/lucasgranade/mazeapi/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type getMazeGraphTest struct {
	reqMazeID     string
	reqFrom       string
	reqTo         string
	mazeExists    bool
	existentGraph []model.Spot
	statusCode    int
}

var getMazeGraphTests = map[string]getMazeGraphTest{
	"OK": getMazeGraphTest{
		"5fbf3dab715e12693d8d6076",
		"A",
		"B",
		true,
		[]model.Spot{
			model.Spot{
				ID:   primitive.NewObjectID(),
				Name: "A",
				X:    0,
				Y:    0,
				Gold: 10,
				Connects: []model.ConnectedSpot{
					model.ConnectedSpot{
						Spot: "B",
						Path: 10,
					},
				},
			},
			model.Spot{
				ID:   primitive.NewObjectID(),
				Name: "B",
				X:    10,
				Y:    0,
				Gold: 10,
				Connects: []model.ConnectedSpot{
					model.ConnectedSpot{
						Spot: "A",
						Path: 10,
					},
				},
			},
		},
		200,
	},
	"MazeNotFound": getMazeGraphTest{
		"5fbf3dab715e12693d8d6076",
		"A",
		"B",
		false,
		nil,
		404,
	},
	"FromSpotNotFound": getMazeGraphTest{
		"5fbf3dab715e12693d8d6076",
		"NonExistentSpot",
		"B",
		true,
		[]model.Spot{
			model.Spot{
				ID:   primitive.NewObjectID(),
				Name: "A",
				X:    0,
				Y:    0,
				Gold: 10,
				Connects: []model.ConnectedSpot{
					model.ConnectedSpot{
						Spot: "B",
						Path: 10,
					},
				},
			},
			model.Spot{
				ID:   primitive.NewObjectID(),
				Name: "B",
				X:    10,
				Y:    0,
				Gold: 10,
				Connects: []model.ConnectedSpot{
					model.ConnectedSpot{
						Spot: "A",
						Path: 10,
					},
				},
			},
		},
		404,
	},
	"ToSpotNotFound": getMazeGraphTest{
		"5fbf3dab715e12693d8d6076",
		"A",
		"NonExistentSpot",
		true,
		[]model.Spot{
			model.Spot{
				ID:   primitive.NewObjectID(),
				Name: "A",
				X:    0,
				Y:    0,
				Gold: 10,
				Connects: []model.ConnectedSpot{
					model.ConnectedSpot{
						Spot: "B",
						Path: 10,
					},
				},
			},
			model.Spot{
				ID:   primitive.NewObjectID(),
				Name: "B",
				X:    10,
				Y:    0,
				Gold: 10,
				Connects: []model.ConnectedSpot{
					model.ConnectedSpot{
						Spot: "A",
						Path: 10,
					},
				},
			},
		},
		404,
	},
	"LackingFrom": getMazeGraphTest{
		"5fbf3dab715e12693d8d6076",
		"",
		"B",
		true,
		nil,
		400,
	},
	"LackingTo": getMazeGraphTest{
		"5fbf3dab715e12693d8d6076",
		"A",
		"",
		true,
		nil,
		400,
	},
}

func TestGetMazeMinDistance(t *testing.T) {
	for k, v := range getMazeGraphTests {
		t.Run(k, func(it *testing.T) {

			dao.Set(&dao.MazeMockupImpl{
				ReadGraphHandler: func(ctx context.Context, mazeID primitive.ObjectID) ([]model.Spot, error) {
					if !v.mazeExists {
						return nil, dao.ErrNotFound
					}
					return v.existentGraph, nil
				},
			})

			handler, err := createRoutes()
			if err != nil {
				it.Errorf("%w", err)
				return
			}

			req := httptest.NewRequest(http.MethodGet, fmt.Sprintf("https://api.ovloop.com/mazes/%s/mindistance?from=%s&to=%s", v.reqMazeID, v.reqFrom, v.reqTo), nil)

			w := httptest.NewRecorder()
			handler.ServeHTTP(w, req)
			resp := w.Result()

			assert.Equal(it, v.statusCode, resp.StatusCode, "bad return status code")

			if resp.StatusCode == http.StatusOK {
				decoder := json.NewDecoder(resp.Body)
				route := model.Route{}
				err = decoder.Decode(&route)
				if err != nil {
					it.Errorf("Couldn't parse body, err: %v", err)
					return
				}
			}
		})
	}
}

func TestGetMazeMaxGold(t *testing.T) {

	for k, v := range getMazeGraphTests {
		t.Run(k, func(it *testing.T) {

			dao.Set(&dao.MazeMockupImpl{
				ReadGraphHandler: func(ctx context.Context, mazeID primitive.ObjectID) ([]model.Spot, error) {
					if !v.mazeExists {
						return nil, dao.ErrNotFound
					}
					return v.existentGraph, nil
				},
			})

			handler, err := createRoutes()
			if err != nil {
				it.Errorf("%w", err)
				return
			}

			req := httptest.NewRequest(http.MethodGet, fmt.Sprintf("https://api.ovloop.com/mazes/%s/maxgold?from=%s&to=%s", v.reqMazeID, v.reqFrom, v.reqTo), nil)

			w := httptest.NewRecorder()
			handler.ServeHTTP(w, req)
			resp := w.Result()

			assert.Equal(it, v.statusCode, resp.StatusCode, "bad return status code")

			if resp.StatusCode == http.StatusOK {
				decoder := json.NewDecoder(resp.Body)
				route := model.Route{}
				err = decoder.Decode(&route)
				if err != nil {
					it.Errorf("Couldn't parse body, err: %v", err)
					return
				}
			}
		})
	}
}
