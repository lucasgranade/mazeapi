package main

import (
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi/middleware"
)

type httpLogger struct {
}

type httpEntry struct {
	reqID string
	r     *http.Request
}

func (l *httpLogger) NewLogEntry(r *http.Request) middleware.LogEntry {
	e := &httpEntry{
		r: r,
	}

	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		e.reqID = reqID
	}

	log.Printf("Entering %s %s\n", e.r.Method, e.r.RequestURI)

	return e
}

// Write is called by middleware at the end of the request
func (e *httpEntry) Write(status, bytes int, header http.Header, elapsed time.Duration, extra interface{}) {
	elapsedMs := float64(elapsed.Nanoseconds()) / 1000000.0

	log.Printf("Exiting %s %s %d %f ms\n", e.r.Method, e.r.RequestURI, status, elapsedMs)
}

// Panic is called by the error handler to print errors
// which is called when there is an uncontrolled
// error during the processing of the request
func (e *httpEntry) Panic(v interface{}, stack []byte) {
	log.Printf("Panic %+v\n", v)
	for _, s := range stack {
		log.Printf("%s\n", string(s))
	}
}
