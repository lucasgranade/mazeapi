package dao

import (
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/lucasgranade/mazeapi/model"
)

// Maze interacts with db
type Maze interface {
	CreateMaze(ctx context.Context, name string, center model.Center) (*model.Maze, error)

	ReadMazeByID(ctx context.Context, mazeID primitive.ObjectID) (*model.Maze, error)

	ModifyMaze(ctx context.Context, mazeID primitive.ObjectID, center model.Center) (*model.Maze, error)

	DeleteMaze(ctx context.Context, mazeID primitive.ObjectID) error

	CreateSpot(
		ctx context.Context,
		mazeID primitive.ObjectID,
		name string,
		x float64,
		y float64,
		gold float64,
		connects []model.ConnectedSpot,
	) (*model.Spot, error)

	ReadSpotByID(ctx context.Context, spotID primitive.ObjectID) (*model.Spot, error)

	DeleteSpot(ctx context.Context, spotID primitive.ObjectID) error

	ReadGraph(ctx context.Context, mazeID primitive.ObjectID) ([]model.Spot, error)
}
