package dao

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"
)

var maze Maze

var mongoClient *mongo.Client

var dbName string

// Config holds parameters to initiate mongodb connection
type Config struct {
	ConnectionURI string
	Database      string
}

// Start initializates needed mongodb stuff
func Start(ctx context.Context, c *Config) error {
	if maze != nil {
		return nil
	}

	dbName = c.Database

	clientOpts := options.Client().ApplyURI(c.ConnectionURI).
		// SetReadConcern(readconcern.Snapshot()).
		SetWriteConcern(writeconcern.New(
			writeconcern.W(1),
		))

	var err error
	mongoClient, err = mongo.Connect(ctx, clientOpts)

	if err != nil {
		return err
	}

	maze = &mazeImpl{}
	return nil
}

// Stop disconnects from db
func Stop(ctx context.Context) {
	if mongoClient != nil {
		mongoClient.Disconnect(ctx)
	}
}

// Get returns maze dao manager to read and write from model
func Get() Maze {
	return maze
}

// Set sets a maze object to be used instead of default (real) one
func Set(m Maze) {
	maze = m
}

func isMongoDupKey(err error) bool {
	if wexc, ok := err.(mongo.WriteException); ok {
		for _, we := range wexc.WriteErrors {
			if we.Code == 11000 {
				return true
			}
		}
	}
	return false
}
