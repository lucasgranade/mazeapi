// +build integration,dao

package dao

import (
	"context"
	"flag"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/lucasgranade/mazeapi/model"
)

type TestParams struct {
	mongodbURI *string
	database   *string
}

var testParams = TestParams{
	mongodbURI: flag.String("mongodbUri", "", "mongodb connection uri"),
	database:   flag.String("database", "test", "database name"),
}

// Used only to drop database used in tests
func DropDatabase(ctx context.Context) {
	mongoClient.Database(dbName).Drop(ctx)
}

func TestCrud(t *testing.T) {
	ctx := context.Background()

	err := Start(ctx, &Config{
		ConnectionURI: *testParams.mongodbURI,
		Database:      *testParams.database,
	})
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	defer DropDatabase(ctx)

	maze, err := Get().CreateMaze(ctx, "test", model.Center{X: 0, Y: 0})
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	_, err = Get().CreateSpot(ctx, maze.ID, "A", 0, 0, 0, []model.ConnectedSpot{
		model.ConnectedSpot{
			Spot: "B",
			Path: 10,
		},
	})
	if err != nil {
		t.Errorf("%w", err)
		return
	}
	spotB, err := Get().CreateSpot(ctx, maze.ID, "B", 0, 0, 0, []model.ConnectedSpot{
		model.ConnectedSpot{
			Spot: "A",
			Path: 10,
		},
	})
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	readSpotB, err := Get().ReadSpotByID(ctx, spotB.ID)
	if err != nil {
		t.Errorf("%w", err)
		return
	}
	assert.NotEmpty(t, readSpotB)

	spots, err := Get().ReadGraph(ctx, maze.ID)
	if err == ErrNotFound {
		t.Errorf("%w", err)
		return
	} else if err != nil {
		t.Errorf("%w", err)
		return
	}
	assert.Equal(t, 2, len(spots))

	err = Get().DeleteSpot(ctx, spotB.ID)
	if err != nil {
		t.Errorf("%w", err)
		return
	}
	readSpotB, err = Get().ReadSpotByID(ctx, spotB.ID)
	if err != nil && err != ErrNotFound {
		t.Errorf("%w", err)
		return
	}
	assert.Empty(t, readSpotB)

	readMaze, err := Get().ReadMazeByID(ctx, maze.ID)
	if err != nil {
		t.Errorf("%w", err)
		return
	}
	assert.NotEmpty(t, readMaze)

	err = Get().DeleteMaze(ctx, maze.ID)
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	readMaze, err = Get().ReadMazeByID(ctx, maze.ID)
	if err != nil && err != ErrNotFound {
		t.Errorf("%w", err)
		return
	}
	assert.Empty(t, readMaze)
}

func TestQuadrant(t *testing.T) {
	ctx := context.Background()

	err := Start(ctx, &Config{
		ConnectionURI: *testParams.mongodbURI,
		Database:      *testParams.database,
	})
	if err != nil {
		t.Errorf("%w", err)
		return
	}
	defer DropDatabase(ctx)

	maze, err := Get().CreateMaze(ctx, "test", model.Center{X: 0, Y: 0})
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	spotA, err := Get().CreateSpot(ctx, maze.ID, "A", 0, 0, 0, []model.ConnectedSpot{})
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	readSpotA, err := Get().ReadSpotByID(ctx, spotA.ID)
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	assert.Equal(t, model.UpperRight, readSpotA.Quadrant)

	_, err = Get().ModifyMaze(ctx, maze.ID,
		model.Center{
			X: 10,
			Y: 0,
		},
	)
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	readSpotA, err = Get().ReadSpotByID(ctx, spotA.ID)
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	assert.Equal(t, model.UpperLeft, readSpotA.Quadrant)

	_, err = Get().ModifyMaze(ctx, maze.ID,
		model.Center{
			X: 10,
			Y: 10,
		},
	)
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	readSpotA, err = Get().ReadSpotByID(ctx, spotA.ID)
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	assert.Equal(t, model.LowerLeft, readSpotA.Quadrant)

	_, err = Get().ModifyMaze(ctx, maze.ID,
		model.Center{
			X: -10,
			Y: 10,
		},
	)
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	readSpotA, err = Get().ReadSpotByID(ctx, spotA.ID)
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	assert.Equal(t, model.LowerRight, readSpotA.Quadrant)
}

func TestEarlyAndLateConnections(t *testing.T) {
	ctx := context.Background()

	err := Start(ctx, &Config{
		ConnectionURI: *testParams.mongodbURI,
		Database:      *testParams.database,
	})
	if err != nil {
		t.Errorf("%w", err)
		return
	}
	defer DropDatabase(ctx)

	maze, err := Get().CreateMaze(ctx, "test", model.Center{X: 0, Y: 0})
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	spotA, err := Get().CreateSpot(ctx, maze.ID, "A", 0, 0, 0, []model.ConnectedSpot{
		model.ConnectedSpot{
			Spot: "A",
			Path: 0,
		},
		model.ConnectedSpot{
			Spot: "B",
			Path: 10,
		},
	})
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	assert.Equal(t, 0, len(spotA.Connects))

	readSpotA, err := Get().ReadSpotByID(ctx, spotA.ID)
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	assert.Equal(t, 0, len(readSpotA.Connects))

	spotB, err := Get().CreateSpot(ctx, maze.ID, "B", 10, 0, 10, []model.ConnectedSpot{
		model.ConnectedSpot{
			Spot: "A",
			Path: 10,
		},
	})
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	readSpotA, err = Get().ReadSpotByID(ctx, spotA.ID)
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	if assert.Equal(t, 1, len(readSpotA.Connects)) {
		assert.Equal(t, "B", readSpotA.Connects[0].Spot)
		assert.Equal(t, 10., readSpotA.Connects[0].Path)
	}

	err = Get().DeleteSpot(ctx, spotB.ID)
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	readSpotA, err = Get().ReadSpotByID(ctx, spotA.ID)
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	assert.Equal(t, 0, len(readSpotA.Connects))
}

func TestDuplicateSpot(t *testing.T) {
	ctx := context.Background()

	err := Start(ctx, &Config{
		ConnectionURI: *testParams.mongodbURI,
		Database:      *testParams.database,
	})
	if err != nil {
		t.Errorf("%w", err)
		return
	}
	defer DropDatabase(ctx)

	maze, err := Get().CreateMaze(ctx, "test", model.Center{X: 0, Y: 0})
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	_, err = Get().CreateSpot(ctx, maze.ID, "A", 0, 0, 0, []model.ConnectedSpot{})
	if err != nil {
		t.Errorf("%w", err)
		return
	}

	_, err = Get().CreateSpot(ctx, maze.ID, "A", 0, 0, 0, []model.ConnectedSpot{})
	assert.Equal(t, ErrSpotNameAlreadyUsed, err)
}
