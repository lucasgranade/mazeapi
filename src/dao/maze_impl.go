package dao

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"gitlab.com/lucasgranade/mazeapi/model"
	"go.mongodb.org/mongo-driver/bson"
)

const (
	mazesCollection = "mazes"
	spotsCollection = "spots"
)

type mazeImpl struct {
}

func (mi *mazeImpl) createIndexes(ctx context.Context) error {
	collection := mongoClient.Database(dbName).Collection(spotsCollection)
	indexes := make(map[string]bool)
	indexView := collection.Indexes()
	cursor, err := indexView.List(ctx)
	for cursor.Next(ctx) {
		var index bson.M
		if err = cursor.Decode(&index); err != nil {
			return err
		}
		if index["key"].(bson.M)["name"] != nil {
			indexes["name"] = true
		}
	}

	if !indexes["name"] {
		// log.Println("Creating unique name index on spots collection ...")
		_, err := collection.Indexes().CreateOne(ctx,
			mongo.IndexModel{
				Keys:    bsonx.Doc{{"mazeId", bsonx.Int32(1)}, {"name", bsonx.Int32(1)}},
				Options: options.Index().SetUnique(true),
			})
		if err != nil {
			return err
		}
	}

	return nil
}

func (mi *mazeImpl) CreateMaze(ctx context.Context, name string, center model.Center) (*model.Maze, error) {

	err := mi.createIndexes(ctx)
	if err != nil {
		log.Println("Can't create indexes, err: ", err)
		return nil, ErrInternal
	}

	collection := mongoClient.Database(dbName).Collection(mazesCollection)

	res, err := collection.InsertOne(ctx, bson.M{
		"name":   name,
		"center": center,
	})
	if err != nil {
		log.Println("Error in db: ", err)
		return nil, ErrInternal
	}
	id := res.InsertedID.(primitive.ObjectID)
	return &model.Maze{
		ID:     id,
		Name:   name,
		Center: center,
	}, nil
}

func (mi *mazeImpl) ReadMazeByID(ctx context.Context, mazeID primitive.ObjectID) (*model.Maze, error) {
	collection := mongoClient.Database(dbName).Collection(mazesCollection)

	cursor, err := collection.Find(ctx, bson.M{
		"_id": mazeID,
	})
	defer cursor.Close(ctx)
	if err != nil {
		log.Println("Error searching maze by id in db: ", err)
		return nil, ErrInternal
	}

	if !cursor.Next(ctx) {
		return nil, ErrNotFound
	}
	maze := &model.Maze{}
	err = cursor.Decode(maze)
	if err != nil {
		log.Println("Error in db: ", err)
		return nil, ErrInternal
	}

	return maze, nil
}

func (mi *mazeImpl) ModifyMaze(ctx context.Context, mazeID primitive.ObjectID, center model.Center) (*model.Maze, error) {
	collection := mongoClient.Database(dbName).Collection(mazesCollection)

	_, err := collection.UpdateOne(
		ctx,
		bson.M{
			"_id": bson.M{
				"$eq": mazeID,
			},
		},
		bson.M{
			"$set": bson.M{
				"center": center,
			},
		},
	)
	if err != nil {
		log.Println("Error in db: ", err)
		return nil, ErrInternal
	}

	return mi.ReadMazeByID(ctx, mazeID)
}

func (mi *mazeImpl) DeleteMaze(ctx context.Context, mazeID primitive.ObjectID) error {
	collection := mongoClient.Database(dbName).Collection(spotsCollection)
	_, err := collection.DeleteMany(ctx, bson.M{
		"mazeId": mazeID,
	})
	if err != nil {
		log.Println("Error in db: ", err)
		return ErrInternal
	}

	collection = mongoClient.Database(dbName).Collection(mazesCollection)
	_, err = collection.DeleteOne(ctx, bson.M{
		"_id": mazeID,
	})
	if err != nil {
		log.Println("Error in db: ", err)
		return ErrInternal
	}

	return nil
}

func (mi *mazeImpl) CreateSpot(
	ctx context.Context,
	mazeID primitive.ObjectID,
	name string,
	x float64,
	y float64,
	gold float64,
	connects []model.ConnectedSpot,
) (*model.Spot, error) {

	maze, err := mi.ReadMazeByID(ctx, mazeID)
	if err == ErrNotFound {
		return nil, ErrNotFound
	} else if err != nil {
		log.Println("Error in db: ", err)
		return nil, ErrInternal
	}

	collection := mongoClient.Database(dbName).Collection(spotsCollection)

	if connects != nil && len(connects) > 0 {
		var in bson.A
		for _, c := range connects {
			in = append(in, c.Spot)
		}

		cursor, err := collection.Find(ctx, bson.M{
			"mazeId": mazeID,
			"name": bson.M{
				"$in": in,
			},
		})
		defer cursor.Close(ctx)
		if err != nil {
			log.Println("Error searching spots in db: ", err)
			return nil, ErrInternal
		}

		existentSpots := make(map[string]bool)
		for cursor.Next(ctx) {
			existentSpot := &model.Spot{}
			err = cursor.Decode(existentSpot)
			if err != nil {
				log.Println("err decoding answer from db: ", err)
				return nil, ErrInternal
			}
			existentSpots[existentSpot.Name] = true
		}

		filteredConnects := []model.ConnectedSpot{}
		for _, c := range connects {
			if _, ok := existentSpots[c.Spot]; ok {
				filteredConnects = append(filteredConnects, c)
			}
		}
		connects = filteredConnects
	}

	res, err := collection.InsertOne(ctx, bson.M{
		"mazeId":   mazeID,
		"name":     name,
		"x":        x,
		"y":        y,
		"gold":     gold,
		"connects": connects,
	})
	if isMongoDupKey(err) {
		return nil, ErrSpotNameAlreadyUsed
	} else if err != nil {
		log.Println("Error in db: ", err)
		return nil, ErrInternal
	}

	id := res.InsertedID.(primitive.ObjectID)
	spot := &model.Spot{
		ID:       id,
		Name:     name,
		X:        x,
		Y:        y,
		Gold:     gold,
		Connects: connects,
	}

	// we add connections to the new spot from the pre existent spots
	for _, con := range connects {
		_, err := collection.UpdateOne(
			ctx,
			bson.M{
				"mazeId": bson.M{
					"$eq": mazeID,
				},
				"name": bson.M{
					"$eq": con.Spot,
				},
			},
			bson.M{
				"$push": bson.M{
					"connects": model.ConnectedSpot{
						Spot: name,
						Path: con.Path,
					},
				},
			},
		)
		if err != nil {
			log.Println("Error in db: ", err)
			return nil, ErrInternal
		}
	}

	spot.CalculateQuadrant(maze)
	return spot, nil
}

func (mi *mazeImpl) ReadSpotByID(ctx context.Context, spotID primitive.ObjectID) (*model.Spot, error) {
	collection := mongoClient.Database(dbName).Collection(spotsCollection)

	cursor, err := collection.Find(ctx, bson.M{
		"_id": spotID,
	})
	defer cursor.Close(ctx)
	if err != nil {
		log.Println("Error searching spot by id in db: ", err)
		return nil, ErrInternal
	}

	if !cursor.Next(ctx) {
		return nil, ErrNotFound
	}
	spot := &model.Spot{}
	err = cursor.Decode(spot)
	if err != nil {
		return nil, ErrInternal
	}

	maze, err := mi.ReadMazeByID(ctx, spot.MazeID)
	if err != nil {
		return nil, ErrInternal
	}
	spot.CalculateQuadrant(maze)

	return spot, nil
}

func (mi *mazeImpl) DeleteSpot(ctx context.Context, spotID primitive.ObjectID) error {
	spot, err := mi.ReadSpotByID(ctx, spotID)
	if err == ErrNotFound {
		return nil
	} else if err != nil {
		return ErrInternal
	}

	collection := mongoClient.Database(dbName).Collection(spotsCollection)

	_, err = collection.DeleteOne(ctx, bson.M{
		"_id": spotID,
	})
	if err != nil {
		log.Println("Error in db: ", err)
		return ErrInternal
	}

	// we remove connections to the deleted spot from the remaining spots that were connected to it
	for _, con := range spot.Connects {
		_, err := collection.UpdateOne(
			ctx,
			bson.M{
				"mazeId": bson.M{
					"$eq": spot.MazeID,
				},
				"name": bson.M{
					"$eq": con.Spot,
				},
			},
			bson.M{
				"$pull": bson.M{
					"connects": bson.M{
						"spot": spot.Name,
					},
				},
			},
		)
		if err != nil {
			log.Println("Error in db: ", err)
			return ErrInternal
		}
	}

	return nil
}

func (mi *mazeImpl) ReadGraph(ctx context.Context, mazeID primitive.ObjectID) ([]model.Spot, error) {
	maze, err := mi.ReadMazeByID(ctx, mazeID)
	if err == ErrNotFound {
		return nil, ErrNotFound
	} else if err != nil {
		return nil, ErrInternal
	}

	collection := mongoClient.Database(dbName).Collection(spotsCollection)

	cursor, err := collection.Find(ctx, bson.M{
		"mazeId": mazeID,
	})
	defer cursor.Close(ctx)
	if err != nil {
		log.Println("Error searching maze graph in db: ", err)
		return nil, ErrInternal
	}

	var spots []model.Spot
	for i := 0; cursor.Next(ctx); i++ {
		spots = append(spots, model.Spot{})
		err = cursor.Decode(&spots[i])
		if err != nil {
			log.Println("Error searching maze graph in db: ", err)
			return nil, ErrInternal
		}
		spots[i].CalculateQuadrant(maze)
	}

	return spots, nil
}
