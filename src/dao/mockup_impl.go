package dao

import (
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/lucasgranade/mazeapi/model"
)

// MazeMockupImpl simulates all maze dao methods to ease unit testing without
// using a real mongo db
type MazeMockupImpl struct {
	CreateMazeHandler func(ctx context.Context, name string, center model.Center) (*model.Maze, error)

	ReadMazeByIDHandler func(ctx context.Context, mazeID primitive.ObjectID) (*model.Maze, error)

	ModifyMazeHandler func(ctx context.Context, mazeID primitive.ObjectID, center model.Center) (*model.Maze, error)

	DeleteMazeHandler func(ctx context.Context, mazeID primitive.ObjectID) error

	CreateSpotHandler func(ctx context.Context, mazeID primitive.ObjectID, name string, x float64, y float64, gold float64, connects []model.ConnectedSpot) (*model.Spot, error)

	ReadSpotByIDHandler func(ctx context.Context, spotID primitive.ObjectID) (*model.Spot, error)

	ReadGraphHandler func(ctx context.Context, mazeID primitive.ObjectID) ([]model.Spot, error)
}

// CreateMaze simulates creatin a maze
func (mi *MazeMockupImpl) CreateMaze(ctx context.Context, name string, center model.Center) (*model.Maze, error) {
	if mi.CreateMazeHandler != nil {
		return mi.CreateMazeHandler(ctx, name, center)
	}
	return nil, nil
}

// ReadMazeByID simulates reading a maze
func (mi *MazeMockupImpl) ReadMazeByID(ctx context.Context, mazeID primitive.ObjectID) (*model.Maze, error) {
	if mi.ReadMazeByIDHandler != nil {
		return mi.ReadMazeByIDHandler(ctx, mazeID)
	}
	return nil, nil
}

// ModifyMaze simulates modifying a maze
func (mi *MazeMockupImpl) ModifyMaze(ctx context.Context, mazeID primitive.ObjectID, center model.Center) (*model.Maze, error) {
	if mi.ModifyMazeHandler != nil {
		return mi.ModifyMazeHandler(ctx, mazeID, center)
	}
	return nil, nil
}

// DeleteMaze simulates deleting a maze
func (mi *MazeMockupImpl) DeleteMaze(ctx context.Context, mazeID primitive.ObjectID) error {
	if mi.DeleteMazeHandler != nil {
		return mi.DeleteMazeHandler(ctx, mazeID)
	}
	return nil
}

// CreateSpot simulates creating a spot
func (mi *MazeMockupImpl) CreateSpot(
	ctx context.Context,
	mazeID primitive.ObjectID,
	name string,
	x float64,
	y float64,
	gold float64,
	connects []model.ConnectedSpot,
) (*model.Spot, error) {
	if mi.CreateSpotHandler != nil {
		return mi.CreateSpotHandler(ctx, mazeID, name, x, y, gold, connects)
	}
	return nil, nil
}

// ReadSpotByID simulates reading a spot
func (mi *MazeMockupImpl) ReadSpotByID(ctx context.Context, spotID primitive.ObjectID) (*model.Spot, error) {
	if mi.ReadSpotByIDHandler != nil {
		return mi.ReadSpotByIDHandler(ctx, spotID)
	}
	return nil, nil
}

// DeleteSpot simulates deleting a spot
func (mi *MazeMockupImpl) DeleteSpot(ctx context.Context, spotID primitive.ObjectID) error {
	return nil
}

// ReadGraph simulates reading a graph
func (mi *MazeMockupImpl) ReadGraph(ctx context.Context, mazeID primitive.ObjectID) ([]model.Spot, error) {
	if mi.ReadGraphHandler != nil {
		return mi.ReadGraphHandler(ctx, mazeID)
	}
	return nil, nil
}
