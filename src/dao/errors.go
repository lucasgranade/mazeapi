package dao

import "errors"

// ErrInternal is an unexplained error
var ErrInternal = errors.New("Internal DB Error")

// ErrNotFound is returned when the object is not found in db
var ErrNotFound = errors.New("Not Found Error")

// ErrSpotNameAlreadyUsed is returned when trying to create spot with already used name
var ErrSpotNameAlreadyUsed = errors.New("Spot Name Already Used")
