// +build !unit !integration
// Only runs as daemon, skipped at unit tests

package main

import "os"

func init() {
	params.MongoDBURI = os.Getenv("MONGODB_URI")
	params.Database = os.Getenv("DATABASE")
}
