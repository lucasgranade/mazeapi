// +build unit

package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/lucasgranade/mazeapi/dao"
	"gitlab.com/lucasgranade/mazeapi/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type postSpotTest struct {
	reqMazeID   string
	reqBody     string
	mazeExists  bool
	statusCode  int
	spotCreated bool
}

var postSpotTests = map[string]postSpotTest{
	"OK": postSpotTest{
		"5fbf3dab715e12693d8d6076",
		`
		{
			"name": "exit",
			"x": 0,
			"y": 0,
			"gold": 100,
			"connects": [
				{
					"spot": "horror",
					"path": 100
				}
			]
		}
		`,
		true,
		201,
		true,
	},
	"MazeNotFound": postSpotTest{
		"5fbf3dab715e12693d8d6076",
		`
		{
			"name": "exit",
			"x": 0,
			"y": 0,
			"gold": 100,
			"connects": [
				{
					"spot": "horror",
					"path": 100
				}
			]
		}
		`,
		false,
		404,
		false,
	},
	"NoName": postSpotTest{
		"5fbf3dab715e12693d8d6076",
		`
		{
			"x": 0,
			"y": 0,
			"gold": 100,
			"connects": [
				{
					"spot": "horror",
					"path": 100
				}
			]
		}
		`,
		true,
		400,
		false,
	},
	"NoX": postSpotTest{
		"5fbf3dab715e12693d8d6076",
		`
		{
			"name": "exit",
			"y": 0,
			"gold": 100,
			"connects": [
				{
					"spot": "horror",
					"path": 100
				}
			]
		}
		`,
		true,
		400,
		false,
	},
	"BadY": postSpotTest{
		"5fbf3dab715e12693d8d6076",
		`
		{
			"name": "exit",
			"x": 0,
			"y": "bad",
			"gold": 100,
			"connects": [
				{
					"spot": "horror",
					"path": 100
				}
			]
		}
		`,
		true,
		400,
		false,
	},
	"BadGold": postSpotTest{
		"5fbf3dab715e12693d8d6076",
		`
		{
			"name": "exit",
			"x": 0,
			"y": 0,
			"gold": -100,
			"connects": [
				{
					"spot": "horror",
					"path": 100
				}
			]
		}
		`,
		true,
		400,
		false,
	},
}

func TestPostSpot(t *testing.T) {

	for k, v := range postSpotTests {
		t.Run(k, func(it *testing.T) {

			spotCreated := false

			dao.Set(&dao.MazeMockupImpl{
				CreateSpotHandler: func(ctx context.Context, mazeID primitive.ObjectID, name string, x float64, y float64, gold float64, connects []model.ConnectedSpot) (*model.Spot, error) {

					if !v.mazeExists {
						return nil, dao.ErrNotFound
					}

					spotCreated = true
					return &model.Spot{
						ID:       primitive.NewObjectID(),
						MazeID:   mazeID,
						Name:     name,
						X:        x,
						Y:        y,
						Gold:     gold,
						Connects: connects,
					}, nil
				},
			})

			handler, err := createRoutes()
			if err != nil {
				it.Errorf("%w", err)
				return
			}

			req := httptest.NewRequest(http.MethodPost, fmt.Sprintf("https://api.ovloop.com/mazes/%s/spots", v.reqMazeID), nil)
			req.Body = ioutil.NopCloser(bytes.NewReader([]byte(v.reqBody)))

			w := httptest.NewRecorder()
			handler.ServeHTTP(w, req)
			resp := w.Result()

			assert.Equal(it, v.statusCode, resp.StatusCode, "bad return status code")
			assert.Equal(it, v.spotCreated, spotCreated, "bad spot created flag")

			if resp.StatusCode == http.StatusCreated {
				decoder := json.NewDecoder(resp.Body)
				spot := model.Spot{}
				err = decoder.Decode(&spot)
				if err != nil {
					it.Errorf("Couldn't parse body, err: %v", err)
					return
				}
			}
		})
	}
}

type getSpotTest struct {
	reqMazeID    string
	reqSpotID    string
	existentSpot model.Spot
	statusCode   int
	spotRead     bool
}

var existentMazeID, _ = primitive.ObjectIDFromHex("5fbf3dab715e12693d8d6076")
var existentSpotID, _ = primitive.ObjectIDFromHex("5fbf3dab715e12693d8d6077")

var getSpotTests = map[string]getSpotTest{
	"OK": getSpotTest{
		"5fbf3dab715e12693d8d6076",
		"5fbf3dab715e12693d8d6077",
		model.Spot{
			ID:     existentSpotID,
			MazeID: existentMazeID,
		},
		200,
		true,
	},
	"SpotNotFound": getSpotTest{
		"5fbf3dab715e12693d8d6076",
		"5fbf3dab715e12693d8d6078",
		model.Spot{
			ID:     existentSpotID,
			MazeID: existentMazeID,
		},
		404,
		true,
	},
	"BadSpotID": getSpotTest{
		"5fbf3dab715e12693d8d6076",
		"5fbf3dab715e12693d8d607R",
		model.Spot{
			ID:     existentSpotID,
			MazeID: existentMazeID,
		},
		400,
		false,
	},
	"DifferentMaze": getSpotTest{
		"5fbf3dab715e12693d8d6078",
		"5fbf3dab715e12693d8d6077",
		model.Spot{
			ID:     existentSpotID,
			MazeID: existentMazeID,
		},
		404,
		true,
	},
}

func TestGetSpot(t *testing.T) {

	for k, v := range getSpotTests {
		t.Run(k, func(it *testing.T) {

			spotRead := false

			dao.Set(&dao.MazeMockupImpl{
				ReadSpotByIDHandler: func(ctx context.Context, spotID primitive.ObjectID) (*model.Spot, error) {
					spotRead = true
					if v.existentSpot.ID != spotID {
						return nil, dao.ErrNotFound
					}
					return &v.existentSpot, nil
				},
			})

			handler, err := createRoutes()
			if err != nil {
				it.Errorf("%w", err)
				return
			}

			req := httptest.NewRequest(http.MethodGet, fmt.Sprintf("https://api.ovloop.com/mazes/%s/spots/%s", v.reqMazeID, v.reqSpotID), nil)

			w := httptest.NewRecorder()
			handler.ServeHTTP(w, req)
			resp := w.Result()

			assert.Equal(it, v.statusCode, resp.StatusCode, "bad return status code")
			assert.Equal(it, v.spotRead, spotRead, "bad spot read flag")

			if resp.StatusCode == http.StatusOK {
				decoder := json.NewDecoder(resp.Body)
				spot := model.Spot{}
				err = decoder.Decode(&spot)
				if err != nil {
					it.Errorf("Couldn't parse body, err: %v", err)
					return
				}
			}
		})
	}
}

type deleteSpotTest struct {
	reqMazeID    string
	reqSpotID    string
	existentSpot model.Spot
	statusCode   int
}

var deleteSpotTests = map[string]deleteSpotTest{
	"OK": deleteSpotTest{
		"5fbf3dab715e12693d8d6076",
		"5fbf3dab715e12693d8d6077",
		model.Spot{
			ID:     existentSpotID,
			MazeID: existentMazeID,
		},
		200,
	},
	"BadSpotID": deleteSpotTest{
		"5fbf3dab715e12693d8d6076",
		"5fbf3dab715e12693d8d607R",
		model.Spot{
			ID:     existentSpotID,
			MazeID: existentMazeID,
		},
		400,
	},
	"SpotNotFound": deleteSpotTest{
		"5fbf3dab715e12693d8d6076",
		"5fbf3dab715e12693d8d6078",
		model.Spot{
			ID:     existentSpotID,
			MazeID: existentMazeID,
		},
		404,
	},
	"DifferentMaze": deleteSpotTest{
		"5fbf3dab715e12693d8d6078",
		"5fbf3dab715e12693d8d6077",
		model.Spot{
			ID:     existentSpotID,
			MazeID: existentMazeID,
		},
		404,
	},
}

func TestDeleteSpot(t *testing.T) {

	for k, v := range deleteSpotTests {
		t.Run(k, func(it *testing.T) {

			dao.Set(&dao.MazeMockupImpl{
				ReadSpotByIDHandler: func(ctx context.Context, spotID primitive.ObjectID) (*model.Spot, error) {
					if v.existentSpot.ID != spotID {
						return nil, dao.ErrNotFound
					}
					return &v.existentSpot, nil
				},
			})

			handler, err := createRoutes()
			if err != nil {
				it.Errorf("%w", err)
				return
			}

			req := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("https://api.ovloop.com/mazes/%s/spots/%s", v.reqMazeID, v.reqSpotID), nil)

			w := httptest.NewRecorder()
			handler.ServeHTTP(w, req)
			resp := w.Result()

			assert.Equal(it, v.statusCode, resp.StatusCode, "bad return status code")

		})
	}
}
