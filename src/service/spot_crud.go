package service

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/lucasgranade/mazeapi/dao"
	"gitlab.com/lucasgranade/mazeapi/model"
)

// CreateSpot creates a spot in the maze
func CreateSpot(
	ctx context.Context,
	mazeID primitive.ObjectID,
	name string,
	x float64,
	y float64,
	gold float64,
	connects []model.ConnectedSpot) (*model.Spot, error) {
	spot, err := dao.Get().CreateSpot(
		ctx,
		mazeID,
		name,
		x,
		y,
		gold,
		connects,
	)
	if err == dao.ErrNotFound {
		return nil, ErrNonexistentMaze
	} else if err == dao.ErrSpotNameAlreadyUsed {
		return nil, ErrDuplicateSpot
	} else if err != nil {
		log.Println("error creating a spot in db: ", err)
		return nil, ErrInternal
	}
	return spot, nil
}

// ReadSpotByID reads a spot by id
func ReadSpotByID(ctx context.Context, mazeID primitive.ObjectID, spotID primitive.ObjectID) (*model.Spot, error) {
	spot, err := dao.Get().ReadSpotByID(ctx, spotID)
	if err == dao.ErrNotFound {
		return nil, ErrNonexistentSpot
	} else if err != nil {
		log.Println("err: ", err)
		return nil, ErrInternal
	}
	if spot.MazeID != mazeID {
		return nil, ErrNonexistentSpot
	}
	return spot, nil
}

// DeleteSpot removes a spot from the maze
func DeleteSpot(ctx context.Context, mazeID primitive.ObjectID, spotID primitive.ObjectID) error {
	spot, err := dao.Get().ReadSpotByID(ctx, spotID)
	if err == dao.ErrNotFound {
		return ErrNonexistentSpot
	} else if err != nil {
		log.Println("err: ", err)
		return ErrInternal
	}
	if spot.MazeID != mazeID {
		return ErrNonexistentSpot
	}

	err = dao.Get().DeleteSpot(ctx, spotID)
	if err != nil {
		log.Println("error deleting a spot in db: ", err)
		return ErrInternal
	}
	return nil
}
