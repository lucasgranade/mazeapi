package service

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/lucasgranade/mazeapi/dao"
	"gitlab.com/lucasgranade/mazeapi/model"
)

// CreateMaze creates a maze
func CreateMaze(ctx context.Context, name string, center model.Center) (*model.Maze, error) {
	maze, err := dao.Get().CreateMaze(ctx, name, center)
	if err != nil {
		log.Println("err: ", err)
		return nil, ErrInternal
	}
	return maze, nil
}

// ReadMazeByID reads a maze by id
func ReadMazeByID(ctx context.Context, mazeID primitive.ObjectID) (*model.Maze, error) {
	maze, err := dao.Get().ReadMazeByID(ctx, mazeID)
	if err == dao.ErrNotFound {
		return nil, ErrNonexistentMaze
	} else if err != nil {
		log.Println("err: ", err)
		return nil, ErrInternal
	}
	return maze, nil
}

// ModifyMaze modifies a maze
func ModifyMaze(ctx context.Context, mazeID primitive.ObjectID, center model.Center) (*model.Maze, error) {
	maze, err := dao.Get().ModifyMaze(ctx, mazeID, center)
	if err == dao.ErrNotFound {
		return nil, ErrNonexistentMaze
	} else if err != nil {
		log.Println("err: ", err)
		return nil, ErrInternal
	}
	return maze, nil
}

// DeleteMaze removes a maze
func DeleteMaze(ctx context.Context, mazeID primitive.ObjectID) error {
	_, err := dao.Get().ReadMazeByID(ctx, mazeID)
	if err == dao.ErrNotFound {
		return ErrNonexistentMaze
	} else if err != nil {
		log.Println("err: ", err)
		return ErrInternal
	}

	err = dao.Get().DeleteMaze(ctx, mazeID)
	if err != nil {
		log.Println("error deleting a maze in db: ", err)
		return ErrInternal
	}
	return nil
}
