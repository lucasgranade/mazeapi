package service

import "errors"

// ErrInternal is an unexplained error
var ErrInternal = errors.New("Internal Service Error")

// ErrNonexistentMaze is returned when the requested maze doesn't exist
var ErrNonexistentMaze = errors.New("Nonexistent Maze Error")

// ErrNonexistentSpot is returned when the requested spot doesn't exist
var ErrNonexistentSpot = errors.New("Nonexistent Spot Error")

// ErrDuplicateSpot is returned when trying to create spot with same name
var ErrDuplicateSpot = errors.New("Duplicate Spot Name")
