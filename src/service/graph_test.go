// +build unit

package service

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/lucasgranade/mazeapi/dao"
	"gitlab.com/lucasgranade/mazeapi/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type graphTest struct {
	existentGraph    []model.Spot
	from             string
	to               string
	expectedRoute    []string
	expectedDistance float64
	expectedGold     float64
	expectedError    error
}

var graphX = []model.Spot{
	model.Spot{
		ID:   primitive.NewObjectID(),
		Name: "A",
		X:    10,
		Y:    10,
		Gold: 10,
		Connects: []model.ConnectedSpot{
			model.ConnectedSpot{
				Spot: "2",
				Path: 10,
			},
			model.ConnectedSpot{
				Spot: "B",
				Path: 10,
			},
			model.ConnectedSpot{
				Spot: "3",
				Path: 10,
			},
		},
	},
	model.Spot{
		ID:   primitive.NewObjectID(),
		Name: "B",
		X:    20,
		Y:    10,
		Gold: 20,
		Connects: []model.ConnectedSpot{
			model.ConnectedSpot{
				Spot: "A",
				Path: 10,
			},
			model.ConnectedSpot{
				Spot: "C",
				Path: 10,
			},
			model.ConnectedSpot{
				Spot: "1",
				Path: 10,
			},
			model.ConnectedSpot{
				Spot: "3",
				Path: 15,
			},
		},
	},
	model.Spot{
		ID:   primitive.NewObjectID(),
		Name: "C",
		X:    30,
		Y:    10,
		Gold: 30,
		Connects: []model.ConnectedSpot{
			model.ConnectedSpot{
				Spot: "B",
				Path: 10,
			},
			model.ConnectedSpot{
				Spot: "D",
				Path: 10,
			},
			model.ConnectedSpot{
				Spot: "1",
				Path: 15,
			},
		},
	},
	model.Spot{
		ID:   primitive.NewObjectID(),
		Name: "D",
		X:    30,
		Y:    0,
		Gold: 30,
		Connects: []model.ConnectedSpot{
			model.ConnectedSpot{
				Spot: "C",
				Path: 10,
			},
			model.ConnectedSpot{
				Spot: "3",
				Path: 20,
			},
		},
	},
	model.Spot{
		ID:   primitive.NewObjectID(),
		Name: "1",
		X:    20,
		Y:    20,
		Gold: 50,
		Connects: []model.ConnectedSpot{
			model.ConnectedSpot{
				Spot: "B",
				Path: 10,
			},
			model.ConnectedSpot{
				Spot: "C",
				Path: 15,
			},
			model.ConnectedSpot{
				Spot: "2",
				Path: 10,
			},
		},
	},
	model.Spot{
		ID:   primitive.NewObjectID(),
		Name: "2",
		X:    10,
		Y:    20,
		Gold: 100,
		Connects: []model.ConnectedSpot{
			model.ConnectedSpot{
				Spot: "1",
				Path: 10,
			},
			model.ConnectedSpot{
				Spot: "A",
				Path: 10,
			},
		},
	},
	model.Spot{
		ID:   primitive.NewObjectID(),
		Name: "3",
		X:    10,
		Y:    0,
		Gold: 200,
		Connects: []model.ConnectedSpot{
			model.ConnectedSpot{
				Spot: "A",
				Path: 10,
			},
			model.ConnectedSpot{
				Spot: "B",
				Path: 15,
			},
			model.ConnectedSpot{
				Spot: "D",
				Path: 20,
			},
		},
	},
	model.Spot{
		ID:   primitive.NewObjectID(),
		Name: "X",
		X:    0,
		Y:    0,
		Gold: 500,
		Connects: []model.ConnectedSpot{
			model.ConnectedSpot{
				Spot: "Y",
				Path: 20,
			},
		},
	},
	model.Spot{
		ID:   primitive.NewObjectID(),
		Name: "Y",
		X:    0,
		Y:    20,
		Gold: 500,
		Connects: []model.ConnectedSpot{
			model.ConnectedSpot{
				Spot: "X",
				Path: 20,
			},
		},
	},
}

var minDistanceTests = map[string]graphTest{
	"ABC": graphTest{
		graphX,
		"A",
		"C",
		[]string{"A", "B", "C"},
		20,
		60,
		nil,
	},
	"CBA": graphTest{
		graphX,
		"C",
		"A",
		[]string{"C", "B", "A"},
		20,
		60,
		nil,
	},
	"DC1": graphTest{
		graphX,
		"D",
		"1",
		[]string{"D", "C", "1"},
		25,
		110,
		nil,
	},
	"BE": graphTest{
		graphX,
		"B",
		"E",
		nil,
		0,
		0,
		ErrNonexistentSpot,
	},
	"AXImpossible": graphTest{
		graphX,
		"A",
		"X",
		nil,
		0,
		0,
		nil,
	},
}

func TestGraphMinDistance(t *testing.T) {
	for k, v := range minDistanceTests {
		t.Run(k, func(it *testing.T) {
			ctx := context.Background()

			dao.Set(&dao.MazeMockupImpl{
				ReadGraphHandler: func(ctx context.Context, mazeID primitive.ObjectID) ([]model.Spot, error) {
					return v.existentGraph, nil
				},
			})

			route, err := FindMinDistanceRoute(ctx, primitive.NewObjectID(), v.from, v.to)
			if v.expectedError != nil {
				if !assert.Equal(it, v.expectedError, err, "bad error") {
					it.Errorf("%w", err)
				}
				return
			}

			if assert.Equal(it, len(v.expectedRoute), len(route.Route), "bad route len") {
				for i, expectedSpotName := range v.expectedRoute {
					assert.Equal(it, expectedSpotName, route.Route[i].Name, fmt.Sprintf("bad spot at position %d", i))
				}
			}
			assert.Equal(it, v.expectedDistance, route.Distance, "bad distance")
			assert.Equal(it, v.expectedGold, route.Gold, "bad gold")
		})
	}
}

var maxGoldTests = map[string]graphTest{
	"A21B3DC": graphTest{
		graphX,
		"A",
		"C",
		[]string{"A", "2", "1", "B", "3", "D", "C"},
		75,
		440,
		nil,
	},
	"CD3B12A": graphTest{
		graphX,
		"C",
		"A",
		[]string{"C", "D", "3", "B", "1", "2", "A"},
		75,
		440,
		nil,
	},
	"DCB3A21": graphTest{
		graphX,
		"D",
		"1",
		[]string{"D", "C", "B", "3", "A", "2", "1"},
		65,
		440,
		nil,
	},
	"BE": graphTest{
		graphX,
		"B",
		"E",
		nil,
		0,
		0,
		ErrNonexistentSpot,
	},
	"AXImpossible": graphTest{
		graphX,
		"A",
		"X",
		nil,
		0,
		0,
		nil,
	},
}

func TestGraphMaxGold(t *testing.T) {

	for k, v := range maxGoldTests {
		t.Run(k, func(it *testing.T) {
			ctx := context.Background()

			dao.Set(&dao.MazeMockupImpl{
				ReadGraphHandler: func(ctx context.Context, mazeID primitive.ObjectID) ([]model.Spot, error) {
					return v.existentGraph, nil
				},
			})

			route, err := FindMaxGoldRoute(ctx, primitive.NewObjectID(), v.from, v.to)
			if v.expectedError != nil {
				if !assert.Equal(it, v.expectedError, err, "bad error") {
					it.Errorf("%w", err)
				}
				return
			}

			if assert.Equal(it, len(v.expectedRoute), len(route.Route), "bad route len") {
				for i, expectedSpotName := range v.expectedRoute {
					assert.Equal(it, expectedSpotName, route.Route[i].Name, fmt.Sprintf("bad spot at position %d", i))
				}
			}
			assert.Equal(it, v.expectedDistance, route.Distance, "bad distance")
			assert.Equal(it, v.expectedGold, route.Gold, "bad gold")
		})
	}
}
