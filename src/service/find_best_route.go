package service

import (
	"context"
	"math"

	"gitlab.com/lucasgranade/mazeapi/dao"
	"gitlab.com/lucasgranade/mazeapi/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type vertex struct {
	spot      *model.Spot
	connects  map[string]*vertex
	distances map[string]float64
}

type graph struct {
	vertexes    map[string]*vertex
	minDistance float64
	maxGold     float64
	bestPath    []string
}

// creates graph and indexes model to be iterated
func newGraph(spots []model.Spot) *graph {
	g := &graph{
		vertexes:    make(map[string]*vertex),
		minDistance: math.MaxFloat64,
		bestPath:    []string{},
	}

	for i := range spots {
		s := &spots[i]
		v := &vertex{
			spot:      s,
			connects:  make(map[string]*vertex),
			distances: make(map[string]float64),
		}
		g.vertexes[s.Name] = v
		for _, cSpot := range s.Connects {
			if oVertex, ok := g.vertexes[cSpot.Spot]; ok {
				v.connects[cSpot.Spot] = oVertex
				v.distances[cSpot.Spot] = cSpot.Path
				oVertex.connects[s.Name] = v
				oVertex.distances[s.Name] = cSpot.Path
			}
		}
	}
	return g
}

func (g *graph) findMinDistancePath(from string, to string) (*model.Route, error) {
	rootVertex, ok := g.vertexes[from]
	if !ok {
		return nil, ErrNonexistentSpot
	}
	destVertex, ok := g.vertexes[to]
	if !ok {
		return nil, ErrNonexistentSpot
	}

	path := []string{rootVertex.spot.Name}
	visited := make(map[string]bool)
	visited[rootVertex.spot.Name] = true
	g.deepMinDistance(destVertex, 0, path, visited, rootVertex)

	route := &model.Route{
		Distance: g.minDistance,
		Route:    make([]model.RouteSpot, len(g.bestPath)),
	}
	for i, name := range g.bestPath {
		s := g.vertexes[name].spot
		route.Gold += s.Gold
		route.Route[i].ID = s.ID
		route.Route[i].Name = s.Name
	}
	if len(route.Route) == 0 {
		route.Distance = 0
	}
	return route, nil
}

// recursive solution was used to develope it faster, with more effort I could do a more
// performant non-recursive alternative
func (g *graph) deepMinDistance(destVertex *vertex, currentDistance float64, currentPath []string, visited map[string]bool, v *vertex) {
	for _, oV := range v.connects {
		if !visited[oV.spot.Name] {
			newVisited := make(map[string]bool)
			for key, value := range visited {
				newVisited[key] = value
			}
			newVisited[oV.spot.Name] = true
			newDistance := currentDistance + v.distances[oV.spot.Name]
			newPath := append(currentPath, oV.spot.Name)

			if oV.spot.Name == destVertex.spot.Name {
				if newDistance < g.minDistance {
					g.minDistance = newDistance
					g.bestPath = newPath
				}
			} else {
				g.deepMinDistance(
					destVertex,
					newDistance,
					newPath,
					newVisited,
					oV)
			}
		}
	}
	return
}

func (g *graph) findMaxGoldPath(from string, to string) (*model.Route, error) {
	rootVertex, ok := g.vertexes[from]
	if !ok {
		return nil, ErrNonexistentSpot
	}
	destVertex, ok := g.vertexes[to]
	if !ok {
		return nil, ErrNonexistentSpot
	}

	path := []string{rootVertex.spot.Name}
	visited := make(map[string]bool)
	visited[rootVertex.spot.Name] = true
	g.deepMaxGold(destVertex, rootVertex.spot.Gold, path, visited, rootVertex)

	route := &model.Route{
		Gold:  g.maxGold,
		Route: make([]model.RouteSpot, len(g.bestPath)),
	}
	for i, name := range g.bestPath {
		s := g.vertexes[name].spot
		if i+1 < len(g.bestPath) {
			route.Distance += g.vertexes[name].distances[g.bestPath[i+1]]
		}
		route.Route[i].ID = s.ID
		route.Route[i].Name = s.Name
	}
	if len(route.Route) == 0 {
		route.Distance = 0
	}
	return route, nil
}

// recursive solution was used to develope it faster, with more effort I could do a more
// performant non-recursive alternative
func (g *graph) deepMaxGold(destVertex *vertex, currentGold float64, currentPath []string, visited map[string]bool, v *vertex) {
	for _, oV := range v.connects {
		if !visited[oV.spot.Name] {
			newVisited := make(map[string]bool)
			for key, value := range visited {
				newVisited[key] = value
			}
			newVisited[oV.spot.Name] = true
			newGold := currentGold + oV.spot.Gold
			newPath := append(currentPath, oV.spot.Name)

			if oV.spot.Name == destVertex.spot.Name {
				if newGold > g.maxGold {
					g.maxGold = newGold
					g.bestPath = newPath
				}
			} else {
				g.deepMaxGold(
					destVertex,
					newGold,
					newPath,
					newVisited,
					oV)
			}
		}
	}
	return
}

// FindMinDistanceRoute searches for the shortest path from A to B
func FindMinDistanceRoute(
	ctx context.Context,
	mazeID primitive.ObjectID,
	fromSpot string,
	toSpot string) (*model.Route, error) {

	spots, err := dao.Get().ReadGraph(ctx, mazeID)
	if err == dao.ErrNotFound {
		return nil, ErrNonexistentMaze
	} else if err != nil {
		return nil, ErrInternal
	}

	g := newGraph(spots)

	return g.findMinDistancePath(fromSpot, toSpot)
}

// FindMaxGoldRoute searches for the max gold path from A to B
func FindMaxGoldRoute(
	ctx context.Context,
	mazeID primitive.ObjectID,
	fromSpot string,
	toSpot string) (*model.Route, error) {

	spots, err := dao.Get().ReadGraph(ctx, mazeID)
	if err == dao.ErrNotFound {
		return nil, ErrNonexistentMaze
	} else if err != nil {
		return nil, ErrInternal
	}

	g := newGraph(spots)

	return g.findMaxGoldPath(fromSpot, toSpot)
}
