# Maze API

This is a dockerized Maze API.

## API Contract

The openapi spec is in `/design/contract.yaml`

There is an insomnia file to manually interact with the api in `/design/insomnia.json`

## Building

For help about how to deal with the repo just run:

```bash
$ make
```

All .config_* files should be copied to config_* files and edited as necessary before running any of the following make targets.

### Unit tests

```bash
$ make unit-test-local
```

This test runs locally and standalone (no external dependency). It focuses in auiditing the following things:

* HTTP Routes
* HTTP Errors
* HTTP Controllers (input validations, etc.)
* Service DAO Wrappers
* Service Graph Algorithms

### DAO Integration tests

```bash
$ make dao-test-local
```

This test runs locally and focuses in auditing all crud operations (consistency) with an actual mongodb. It has an external mongodb as a dependency to run. Parameters are passed as flags. Read Makefile.

### Binary

```bash
$ make build-local
```

### Image

```bash
$ make build
```

## Running

To run this you will need a mongodb server running and set the following env variables to the container:

* MONGODB_URI: mongo db connection uri like mongodb://root:password@localhost:27017. This uri indicates user, password, host and port.

* DATABASE: name of the database that the api will use to store the collections. This db is created dunamically by the api.

Look at the Makefile there are examples of how to run the api.

## MongoDB Shema

The api creates the following collections:

* mazes: stores mazes with it's corresponding center (used to determinate quadrant field of spots at read time).

* spots: stores spots with fields indicating connection properties and such things.

## Characteristics

* Ability to create multiple mazes.

* Ability to change maze center. That will decide to which quadrant each spot belongs to.

* Ability to search best route from A to B by walking the least amount of distance.

* Ability to search best route from A to B by getting the most amount of gold.

* Spot names can't be repeated within a maze.

* Inmune to inconsistent connections when creating and deleting spots.

## Implementation Notes

* API has no auth, since it's just a challenge.

## Playing with the API

You can build it and deploy it yourself locally or use the one I already deployed in the cloud (http://18.191.144.246:8080).

* Create a maze

* Create interconnected spots within that maze

* Run mindistance queries to find the shortest path from one spot to another.

* Run maxgold queries to find the best path to gather as much gold as possible going from one spot to another.
